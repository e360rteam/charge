#!/bin/bash

set -e
set -x

#docker rm ladestation -f

cd $(dirname $0)

docker run --rm -ti -v $PWD:/data ladestationenstatus:latest
