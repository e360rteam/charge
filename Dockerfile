FROM rocker/tidyverse:latest

RUN apt-get update
RUN apt-get upgrade -y

# install spatial libs
RUN apt-get -y install libudunits2-dev gdal-bin proj-bin libgdal-dev libproj-dev

# install odbc system libs
RUN apt install -y unixodbc unixodbc-dev
RUN apt install -y tdsodbc

# install CRAN packages
RUN R -q -e 'install.packages("remotes")'
RUN R -q -e 'Sys.setenv(MAKEFLAGS = "-j 2"); update.packages(ask = FALSE)'
RUN R -q -e 'Sys.setenv(MAKEFLAGS = "-j 2"); remotes::install_github("krlmlr/dm@r-0.1.1")'

# install dependencies listed in "charge" DESCR file
COPY DESCRIPTION /data/DESCRIPTION
RUN R -q -e 'Sys.setenv(MAKEFLAGS = "-j 2"); remotes::install_deps("/data")'

# DB connection ----------------------------------------------------------------
COPY inst/odbc.ini /etc/odbc.ini
COPY inst/odbcinst.ini /etc/odbcinst.ini
# UID and PW for database
COPY .Renviron /

CMD [ "/bin/bash" ]
