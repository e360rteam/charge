library(tidyverse)
library(jsonlite)
library(DBI)

pkgload::load_all(dirname(whereami::thisfile()))

timestamp <- Sys.time()
static <- fromJSON("https://data.geo.admin.ch/ch.bfe.ladestellen-elektromobilitaet/data/oicp/ch.bfe.ladestellen-elektromobilitaet.json",
                     simplifyVector = TRUE,
                     simplifyDataFrame = TRUE,
                   flatten = TRUE) %>%
  deep_as_tibble()


static$evse_data %>%
  mutate(evse_data_record = map(evse_data_record, ~mutate(.x, additional_info = NULL))) %>%
  unnest(cols = c(evse_data_record)) %>%
  identity -> all_stations_static

max_power <- function(x) {
  if (nrow(x) == 0 || is.null(x$power)) {
    NA_real_
  } else {
    max(x$power)
  }
}

is_dc_plug <- function(x) {
  any(x %in% c("CHAdeMO", "CCS Combo 2 Plug (Cable Attached)"))
}

energie360_stations <-
  read_csv(
    "data_e360/chargingstations_01.10.2012 - 31.10.2019.csv"
  ) %>%
  janitor::clean_names() %>%
  mutate(evse_id = paste0("CH*SWIEE", station_id))

#energie360°-Stationen, die nicht in den DIEMO Daten enthalten sind
energie360_stations %>%
  anti_join(all_stations_static, by = "evse_id")

go_fast_stations <-
  read_csv(
    "data_e360/gofast_ids_feb_20.csv",
    col_names = c(
      "Location Country", "Location Canton", "Location Name", "Location City",
      "Location Street Name", "Location Street Number2", "Lat", "Lon",
      "Max Power (kw)", "socketType", "Plug Type", "EVSE ID", "Empty"
    ),
    skip = 1
  ) %>%
  janitor::clean_names() %>%
  select(-empty) %>%
  mutate(evse_id = gsub("SWI[*]", "SWIEE", evse_id))

# GoFast-Stationen, die nicht in den DIEMO Daten enthalten sind
go_fast_stations %>%
  anti_join(all_stations_static, by = "evse_id") %>%
  count(evse_id)

all_stations_static %>%
  mutate(charging_facilities = map(charging_facilities, as_tibble)) %>%
  mutate(charging_facilities = map(charging_facilities, function(x) {
    if ("power" %in% names(x)) x$power <- as.numeric(x$power)
    if ("voltage" %in% names(x)) x$voltage <- as.numeric(x$voltage)
    if ("amperage" %in% names(x)) x$amperage <- as.numeric(x$amperage)
    x
  })) %>%
  mutate(max_power_charging_facilities = map_dbl(charging_facilities, max_power)) %>%
  mutate(power = coalesce(max_power_charging_facilities, as.numeric(max_capacity))) %>%
  mutate(has_dc_plug = map_lgl(plugs, is_dc_plug)) %>%
  left_join(go_fast_stations %>% select(evse_id) %>% mutate(operator_name_go_fast = "GoFast"), by = "evse_id") %>%
  mutate(operator_name = coalesce(operator_name_go_fast, operator_name)) %>%
  select(-operator_name_go_fast) %>%
  left_join(energie360_stations %>% select(evse_id) %>% mutate(operator_name_energie360 = "energie360°"), by = "evse_id") %>%
  mutate(operator_name = coalesce(operator_name_energie360, operator_name)) %>%
  select(-operator_name_energie360) %>%
  separate(geo_coordinates_google, into = c("lon", "lat"), sep = ",", convert  = TRUE) %>%
  mutate(lon_new = if_else(between(lat, 5, 11) & between(lon, 45, 48), lat, lon)) %>%
  mutate(lat_new = if_else(between(lat, 5, 11) & between(lon, 45, 48), lon, lat)) %>%
  select(-lon, -lat) %>%
  rename(lon = lon_new, lat = lat_new) %>%
  mutate(timestamp = !!timestamp) %>%
  select(timestamp, evse_id, everything()) %>%
identity -> all_stations_imputed

backup_data(all_stations_imputed, "all_stations.rds")

all_stations_db <-
  all_stations_imputed %>%
  select_if(~ !is.list(.x))

cache_data(all_stations_db, "all_stations_db.rds")

con <- connect_db_neu()
dbAppendTable(con, "static", all_stations_db)
