library(tidyverse)
library(DBI)

pkgload::load_all()

diff_status <- readRDS("schema/diff.rds")

full_status <- readRDS("schema/full.rds")

static <- readRDS("schema/all_stations_db.rds")

heartbeat <- readRDS("schema/heartbeat.rds")

library(dm)
dm <-
  dm(
    heartbeat = heartbeat[0, ], static = static[0, ],
    diff_status = diff_status[0, ], full_status = full_status[0, ]) %>%
  dm_add_pk(heartbeat, timestamp) %>%
  dm_set_colors(red = heartbeat) %>%
  dm_add_fk(diff_status, timestamp, heartbeat) %>%
  dm_add_fk(full_status, timestamp, heartbeat)

dm %>%
  dm_draw()

dm %>%
  dm_nrow()

con <- connect_db_neu()

dm %>%
  names() %>%
  rev() %>%
  paste0("DROP TABLE IF EXISTS ", .) %>%
  map(~ dbExecute(con, .x))

dm_db <-
  dm %>%
  dm_rm_fk(diff_status, evse_id, static) %>%
  dm_rm_fk(full_status, evse_id, static) %>%
  copy_dm_to(con, ., temporary = FALSE)
