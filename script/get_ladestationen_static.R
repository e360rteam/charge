library(tidyverse)
library(jsonlite)
library(stringr)
library(DBI)
library(lubridate)
library(purrr)
library(dbplyr)
library(readxl)

pkgload::load_all()

static <- fromJSON("https://data.geo.admin.ch/ch.bfe.ladestellen-elektromobilitaet/data/oicp/ch.bfe.ladestellen-elektromobilitaet.json",
                simplifyVector = FALSE,
                simplifyDataFrame = TRUE,
                flatten = T)

static$EVSEData %>%
  as_tibble() %>%
  filter(map_int(EVSEDataRecord, nrow) > 0) %>%
identity -> evse_data

evse_data %>%
  select(OperatorID, OperatorName) %>%
identity -> operators

evse_data %>%
  select(OperatorID, data = EVSEDataRecord) %>%
  mutate(data = map(data, mutate_at, vars(AdditionalInfo), as.list)) %>%
  unnest(-OperatorID) %>%
  select_if(is.list)

static_get_all <- function(.data) {

  static_all <- .data %>%
    as.data.frame() %>%
    mutate(AdditionalInfo = NULL) %>%
    mutate(timestamp_static = ymd_hms(Sys.time()))

  return(static_all)
}

static$EVSEData %>%
  as_tibble() %>%
  rowid_to_column("provider_id") %>%
  mutate(EVSEDataRecord = map(EVSEDataRecord, static_get_all)) %>%
  unnest(EVSEDataRecord) %>%
  mutate_if(is.list, as.character) %>%
identity -> all_stations_static

all_stations_static %>%
  mutate(Voltage = ifelse(str_detect(Plugs, "CCS"), "DC",
                          ifelse(str_detect(Plugs, "Type"), "AC",
                                 ifelse(str_detect(Plugs, "CHAdeMO"), "DC", "unbekannt")))) %>%
  identity -> all_stations_static

all_stations_static %>%
  select(Plugs, Voltage) %>%
  group_by(Plugs, Voltage) %>%
  add_count() %>%
  unique() %>%
  view()

#join E360-Anlagen:

chargingstations_01_10_2012_31_10_2019 <- read_csv("data_e360/chargingstations_01.10.2012 - 31.10.2019.csv") %>%
  mutate(Oprt="Energie 360°")

all_stations_static %>%
  mutate(EVSE_short = ifelse(OperatorName == "Swisscharge", str_sub(EvseID, start = -5L), "")) %>%
  mutate(EVSE_short = as.numeric(str_extract(EVSE_short, "(\\d)+"))) %>%
  left_join(chargingstations_01_10_2012_31_10_2019, by = c("EVSE_short" = "Station ID")) %>%
  mutate(OperatorName = ifelse(is.na(Oprt), OperatorName, "Energie 360°")) %>%
  select(-Oprt) %>%
identity ->
  all_stations_static

#Add GoFast Information
GoFast <- read_excel("data_e360/GoFast.xlsx", sheet = "Station") %>%
  select('Station ID') %>%
  mutate(Oprt = "GoFast")

all_stations_static %>%
  left_join(GoFast, by = c("EVSE_short" = "Station ID")) %>%
  mutate(OperatorName = ifelse(is.na(Oprt), OperatorName, "GoFast")) %>%
  select(-Oprt) %>%
  identity ->
  all_stations_static

saveRDS(all_stations_static, "cache/all_stations_static.rds")

#write to database:
tablename <- paste0("all_stations_static_", str_replace_all(ymd(Sys.Date()), "-", ""))

con <- connect_db_new()

dbWriteTable(con, tablename, all_stations_static, overwrite = TRUE)
