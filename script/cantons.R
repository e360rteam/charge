library(tidyverse)
library(jsonlite)
library(stringr)
library(DBI)
library(lubridate)
library(purrr)
library(dbplyr)
library(readxl)

pkgload::load_all()


all_stations_points <- readRDS("cache/all_stations_points.rds")

cantons_multi <-
  sf::read_sf("~/Downloads/SHAPEFILE_LV95_LN02/swissBOUNDARIES3D_1_3_TLM_KANTONSGEBIET.dbf") %>%
  sf::st_transform(2056) %>%
  janitor::clean_names() %>%
  count(kantonsnum, name)

canton_for_point <-
  sf::st_intersects(all_stations_points, cantons_multi) %>%
  as_tibble() %>%
  left_join(tibble(row.id = seq_len(nrow(all_stations_points))), .)

all_stations_points%>%
  mutate(kantonsnum = !!canton_for_point$col.id) %>%
  left_join(cantons_multi %>% as_tibble() %>% select(kantonsnum, name)) %>%
  writexl::write_xlsx("cache/all_stations_imputed.xlsx")
