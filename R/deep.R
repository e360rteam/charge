deep_as_tibble <- function(x) {
  if (is.list(x)) {
    x[] <- map(x[], deep_as_tibble)
  }

  if (is.data.frame(x)) {
    janitor::clean_names(as_tibble(x))
  } else if (is.null(names(x))) {
    x
  } else {
    names(x) <- janitor::make_clean_names(names(x))
    x
  }
}
