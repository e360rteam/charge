cache_dir <- "cache"

cache_data <- function(data, file) {
  file <- file.path(cache_dir, file)
  saveRDS(data, file)
  invisible(file)
}

cache_load <- function(file) {
  file <- file.path(cache_dir, file)
  readRDS(file)
}

cache_has <- function(file) {
  file <- file.path(cache_dir, file)
  file.exists(file)
}
