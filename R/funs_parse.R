get_station_ids <- function(.df) {

  ids_pattern <- "stationids=[A-Za-z0-9*,]+"

  id <- .df[2,] %>%
    str_extract(ids_pattern) %>%
    str_replace("stationids=", "") %>%
    str_split(",") %>%
    as.data.frame(col.names = "id")

  return(id)

}

get_available_status <- function(.df) {

  available_pattern <- "<tr> <th>[A-Za-z0-9äöü ]+</th> </tr>"

  available <- .df[1,] %>%
    str_extract_all(available_pattern) %>%
    as.data.frame(col.names = "Available") %>%
    mutate(Available = str_replace_all(Available, "<tr> <th>", "")) %>%
    mutate(Available = str_replace_all(Available, "</th> </tr>", "")) %>%
    mutate(Available = str_trim(Available))

  return(available)

}

get_plug <- function(.df) {

  plug_pattern <-  "<tr> <td> [A-Za-z0-9äöü ]+ </td> </tr>"

  plug <- .df[1,] %>%
    str_extract_all(plug_pattern) %>%
    as.data.frame(col.names = "Steckertyp") %>%
    mutate(Steckertyp = str_replace_all(Steckertyp, "<tr> <td>", "")) %>%
    mutate(Steckertyp = str_replace_all(Steckertyp, "</td> </tr>", "")) %>%
    mutate(Steckertyp = str_trim(Steckertyp))

  return(plug)

}

get_station_all <- function(.data) {
  station_all <- bind_cols(get_station_ids(.data),
                           #get_plug(.data),
                           get_available_status(.data))
  return(station_all)
}
