
# charge

## Workflow

On a new machine, execute the following steps:

1. `sudo apt install make`
1. `make image`: Creates a docker image named "ladestationenstatus" based on `rocker/verse` (takes around ~ 5 min).
1. `make run`: Calls `run.sh` to run `script/01_status_download.R` in a temporary container which downloads all information of charging stations.
  If everything worked, there should be a new entry in the "e360-charge" database.
1. `make cron`: Create a 1 minute CRON job. Careful: execute only once!)
