#!/bin/bash

set -e
set -x

cd $(dirname $0)

docker run --net=host --rm -v $PWD:/data ladestationenstatus:latest R -q -e 'setwd("/data"); source("script/01_status_download.R", echo = TRUE)'
