---
title: "Analyse Ladestationen"
resource_files:
- charge
runtime: shiny
output: html_document
---

```{r init, include=FALSE}
library(tidyverse)
library(dbplyr)
library(viridisLite)
library(viridis)
library(lubridate)
library(sf)
library(DT)
library(leaflet)
library(leaflet.extras)
library(maps)
library(conflicted)

conflict_prefer("filter", "dplyr")
conflict_prefer("lag", "dplyr")
conflict_prefer("renderDataTable", "DT")
conflict_prefer("map", "purrr")

pkgload::load_all("charge")
```

```{r preprocess, echo=FALSE}

con <- connect_db_new()

status_data <- tbl(con, ident_q("dbo.all_stations_status")) %>%
  rename(EvseID = id)
status_agg_data <- tbl(con, ident_q("dbo.all_stations_agg2"))
static_data <- tbl(con, ident_q("dbo.all_stations_static_20200117"))
static_data_df <- static_data %>% collect()

data <- static_data %>%
  left_join(status_data, by = c("EvseID"))

if (FALSE) {
  data %>%
    select(timestamp) %>%
    arrange(desc(timestamp))
}

station_h_tag <- 
  status_agg_data %>%
  collect() %>%
  mutate(n = as.integer(n)) %>% 
  #complete(tag, EvseID, Available, fill = list(n = 0)) %>%
  group_by(tag, EvseID) %>%
  mutate(slots_tag = sum(n)) %>%
  ungroup() %>%
  mutate(share_tag = n / slots_tag) %>%
  left_join(static_data_df, by = "EvseID")

stationen <- static_data_df %>% 
  select(Address.Street, EvseID, OperatorName, Voltage)

poi <- read.csv2("charge/inst/extdata/ladestationen_mit_poi.csv", fileEncoding = "latin1", sep = ",", stringsAsFactors = FALSE)
```


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```


## Auslastung Ladepunkte {.tabset}

### Operator

Bei dieser App handelt es sich um einen Prototypen. Die Daten stammen vom [Bundesamt für Energie](https://ich-tanke-strom.ch). Alle 15 min werden Ladepunkte in der Schweiz und deren Status (besetzt, frei, etc.) gesammelt und hier in verschiedenen Varianten visualisiert.
Fragen und Anregungen: Team NI, Urs Mändli, Tel. 2176, urs.maendli@energie360.ch  
  
```{r Auslastung, echo=FALSE}
inputPanel(
  selectInput("oprt", label = "Operator:",
              choices = c("Energie 360°",
                          "GoFast",
                          "Eniwa",
                          "evpass",
                          "Move",
                          "PLUG’N ROLL",
                          "Swisscharge"), selected = c("GoFast", "Energie 360°"), multiple = T),
  radioButtons("voltage", label = "Spannung:",
              choices = c("AC",
                          "DC"), selected = c("AC"), inline = TRUE))

renderPlot({
  station_h_tag %>%
    filter(Available == "Besetzt") %>%
    filter(Voltage == !!input$voltage) %>% 
    mutate(stunden_belegt_pro_tag = n/4) %>%
    group_by(tag, OperatorName, Voltage) %>%
    summarise(stunden_total = sum(stunden_belegt_pro_tag)) %>%
    ungroup() %>%
    filter(OperatorName %in% !!input$oprt) %>% 
    ggplot() +
    geom_line(aes(x = tag, y = stunden_total, color = OperatorName)) +
    geom_point(aes(x = tag, y = stunden_total, color = OperatorName))
})
```

folgt:  
- Tesla Type 2 Stecker als DC  
- zusammenhängende Ladepunkte mit unterschiedlicher EvseID kennzeichnen.  
- E360- und GoFast-Ladestationen vollständig zuordnen (laufen z.T. unter swisscharge)  
- Farben für Operator fixieren

### POI
```{r POI, echo=FALSE}
inputPanel(
  selectInput("unterkategorie", label = "Kategorie:",
              choices = read.csv2("charge/inst/extdata/unterkategorien.csv", fileEncoding = "latin1", stringsAsFactors = FALSE), multiple = TRUE))

station_kat <- reactive({
  req(input$unterkategorie)
  
  station_h_tag %>%
    left_join(poi, by = "EvseID") %>% 
    filter(Reduce(`|`, map(!!input$unterkategorie, ~ str_detect(poi_unterkategorie, fixed(.x)))))
})

renderPlot({
  req(nrow(station_kat()))
  
  station_kat() %>% 
    count(EvseID, OperatorName, Voltage) %>% 
    ggplot() +
    geom_bar(aes(x = OperatorName, fill = OperatorName), position = "dodge", alpha = 1) +
    labs(title = "Anzahl Ladepunkte rund um gewählten POI (50 Meter)") +
    facet_wrap(vars(Voltage))
})

renderPlot({
  station_kat() %>%
    filter(Available == "Besetzt") %>% 
    ggplot() +
    geom_bar(aes(x = tag, weight = share_tag / 4, fill = OperatorName), alpha = 1) +
    labs(title = "Belegung in Stunden pro Tag, nach Operator",
         y = "Stunden belegt pro Tag") +
    facet_grid(rows = vars(Voltage), cols = vars(OperatorName))
})
```

### einzelne Ladestationen

```{r Ladestationen}
inputPanel(
  textInput("adresse", label = "Adresse:"),
  dateInput("date", label = "Datum", value = "2019-11-20"),
  selectInput("oprt3", label = "Operator:",
              choices = c("Energie 360°",
                          "GoFast",
                          "Eniwa",
                          "evpass",
                          "Move",
                          "PLUG’N ROLL",
                          "Swisscharge"), selected = c("GoFast", "Energie 360°"), multiple = T)
)

a <- reactive({input$adresse})

renderText({a()})

selection <- reactive({
  if(a() != '') {
    stationen %>%
      filter(OperatorName %in% !!input$oprt3) %>% 
      filter(Reduce(`&`, lapply(strsplit(a(),' ')[[1]], grepl, Address.Street, ignore.case=T)))
  } else {
    stationen %>% 
      filter(OperatorName %in% !!input$oprt3)
  }
})

fill_var <- reactive({
  if(nrow(selection()) >= 10)
    sym("OperatorName")
  else
    sym("EvseID")
})
```


```{r}

renderDataTable({
  selection()
}, height = 400, options = list(pageLength = 6, sDom = '<"top">lrt<"bottom">ip'), filter = "none")

renderPlot({
  req(nrow(selection()))

  selection() %>%
    select(-OperatorName, -Voltage) %>% 
    left_join(station_h_tag, by = "EvseID") %>% 
    filter(Available == "Besetzt") %>%
    filter(tag > !!input$date) %>% 
    mutate(stunden_belegt_pro_tag = n/4) %>% 
    ggplot() +
    geom_col(aes(x = tag, y = stunden_belegt_pro_tag, fill = !!fill_var())) +
    facet_wrap(vars(Voltage))
}, height = 400)
```

### Wochentage

```{r}
renderPlot({
  station_h_tag %>%
    mutate(wochentag = ordered(lubridate::wday(tag, label = TRUE))) %>%
    filter(Available == "Besetzt") %>%
    filter(!is.na(OperatorName)) %>% 
    ggplot() +
    geom_boxplot(aes(x = wochentag, y = share_tag, fill = OperatorName), alpha = 1) +
    facet_wrap(vars(Voltage))
}, height = 400)
```

### Karte

```{r}
inputPanel(
  selectInput("oprt4", label = "Operator:",
              choices = c("Energie 360°",
                          "GoFast",
                          "Eniwa",
                          "evpass",
                          "Move",
                          "PLUG’N ROLL",
                          "Swisscharge"), selected = c("GoFast", "Energie 360°"), multiple = T),
  radioButtons("maptype", label = "Kartentyp:",
               choices = c("Punkte", "Heatmap"), selected = "Punkte"))

map_data <- reactive({
  station_h_tag %>%
    filter(OperatorName %in% !!input$oprt4) %>% 
    mutate(tempcol = str_split(GeoCoordinates.Google, ',')) %>%
    rowwise() %>%
    mutate(y = unlist(tempcol)[1], x = unlist(tempcol)[2]) %>%
    select(-tempcol) %>%
    filter(ChargingStationName != "Ladebox Smart MOVE Mobility SA") %>%
    filter(!EvseID %in% c("CHEVPE2942", "CHEVPE2943")) %>% 
    count(EvseID, OperatorName, Voltage, x, y)
})

map_type <- reactive({
  input$maptype
})

renderLeaflet({
  req(nrow(map_data()))

  if (map_type() == "Heatmap") {
    leaflet(data = map_data()) %>%
      addTiles() %>% 
      addHeatmap(lng = ~as.numeric(y), lat = ~as.numeric(x), intensity = ~n, blur = 50, radius = 20,
                 cellSize = 8, minOpacity = 0.1)
  } else {
    leaflet(data = map_data()) %>%
      addTiles() %>% 
      addCircleMarkers(
        ~ as.numeric(y),
        ~ as.numeric(x),
        color = ~if_else(Voltage == "DC", "#fc8d62", "#66c2a5"),
        clusterOptions = markerClusterOptions(
          showCoverageOnHover = FALSE,
          spiderfyOnMaxZoom = FALSE,
          disableClusteringAtZoom = 11
        )
      )
  }
})
```

