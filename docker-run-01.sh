#!/bin/bash

set -e
set -x

#docker rm ladestation -f

docker run --rm -v $PWD/cache:/data/cache ladestationenstatus:v0.3 R -q -e 'setwd("/data"); source("script/01_status_download.R", echo = TRUE)'
